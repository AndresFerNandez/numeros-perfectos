# Numeros Perfectos

Digamos que un número S es perfecto en base K si es divisible en K y además tanto el resultado de la suma de sus dígitos como la multiplicación son divisibles en K. Si no cumple ninguna de estas condiciones el número es aburrido.