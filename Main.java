
import java.io.BufferedReader;

import java.io.IOException;

import java.io.InputStreamReader;

import java.io.PrintWriter;

import java.math.BigInteger;

import java.util.*;



public class Main {

    static boolean esDivisible(int k, BigInteger s){

        boolean es=false;

        try{

            Integer num =k;

            BigInteger otro = new BigInteger(num.toString());

            BigInteger a=s.mod(otro);

            es= a.toString().equals("0");

        }catch(Exception e){

            

        }

        return es;

    }

    static boolean suma(int k, BigInteger s){

        String a=s.toString();

        int suma=0;

        for(int i=0; i<a.length(); i++){

            suma+=a.charAt(i)-'0';

           //System.out.println(a.charAt(i));

        }

        boolean es=false;

        try{

            es=suma%k==0;

        }catch(Exception e){

            

        }

        return es;

    }

    static boolean mult(int k, BigInteger s){

        String a=s.toString();

        int mult=1;

        for(int i=0; i<a.length(); i++){

            mult*=a.charAt(i)-'0';

        }

        boolean es=false;

        try{

            es=mult%k==0;

        }catch(Exception e){

            

        }

        return es;

    }

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner();

          PrintWriter pr = new PrintWriter(System.out);

           while(sc.hasNext()){

               int k=Integer.parseInt(sc.next());

               BigInteger s=new BigInteger((sc.next()));

               boolean c1=esDivisible(k,s);

               boolean c2=suma(k,s);

               boolean c3=mult(k,s);

               if(c1){

               System.out.print("SI ");

               }else{

               System.out.print("NO ");    

               }

               if(c2){

               System.out.print("SI ");

               }else{

               System.out.print("NO ");    

               }

               if(c3){

               System.out.print("SI");

               }else{

               System.out.print("NO");    

               }

               if(c1&&c2&&c3){

               System.out.print(" PERFECTO");

               }else if(!c1&&!c2&&!c3){

               System.out.print(" ABURRIDO");    

               }

               System.out.println("");

           }

           pr.flush();

       

       pr.close();

        

    }

    static class Scanner {

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer("");

        int spaces = 0;

       

        

        public String nextLine() throws IOException {

            if (spaces-- > 0) {

                return "";

            } else if (st.hasMoreTokens()) {

                return new StringBuilder(st.nextToken("\n")).toString();

            }

            return br.readLine();

        }



        public String next() throws IOException {

            spaces = 0;

            while (!st.hasMoreTokens()) {

                st = new StringTokenizer(br.readLine());

            }

            return st.nextToken();

        }



        public boolean hasNext() throws IOException {

            while (!st.hasMoreTokens()) {

                String line = br.readLine();

                if (line == null) {

                    return false;

                }

                if (line.equals("")) {

                    spaces = Math.max(spaces, 0) + 1;

                }

                st = new StringTokenizer(line);

            }

            return true;

        }

      

           

    }

}